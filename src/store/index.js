  
    import { createGlobalState } from 'react-hooks-global-state';
    import { isMobile } from "react-device-detect";
    import ProductData from '../ProductData';
    import  CustomerData from '../CustomerData'

    const { products } = ProductData;
    const {myCustomers} = CustomerData;


    const { setGlobalState, useGlobalState } = createGlobalState({
      total: 0,
      basketItems:[],
      numberOfCard :0,
      setTab : '',
      searchTerm : "",
      likedProducts : products.filter((item)=>item.liked==true),
      testBrowser: null,
      loginStatus : false,
      customers : CustomerData?.customers,
      currentUser : {},
      myOrder : [],
     
    });

    export const setTotal = () => {
      setGlobalState('total', (v) => v);
     
    };

    export const setBasketItems = () =>{
      setGlobalState('basketItems', (v) => v);
    }
    export const setTab = () =>{
      setGlobalState('setTab', (v) => v);
    } 

    export const setSearchTerm = () =>{
      setGlobalState('searchTerm', (v) => v);
    }
    export const setLikedProducts = () =>{
      setGlobalState('likedProducts', (v) => v);
    }
    export const setTestBrowser = () =>{
      setGlobalState('testBrowser', (v) => v);
    }

    export const setLoginStatus = () =>{
      setGlobalState('loginStatus', (v) => v);
    }
    export const setCustomers = () =>{
      setGlobalState('customers', (v) => v);
    }

    export const setCurrentUser = () =>{
      setGlobalState('currentUser', (v) => v);
    }
    export const  setMyOrder= () =>{
      setGlobalState('myOrder', (v) => v);
    }
    




    export { useGlobalState };