import React, { useState } from 'react';
import { TabBar } from 'antd-mobile';
import { useGlobalState } from '../../../store'
import Home from './../BootomTabPages/Home';
import Search3 from '../Search/Search3';
import Profile from '../Profile/Profile';
import Busket from './../CartAndBusket/Busket';
import Liked from './../Liked/Liked';
import AccountAndProfile from './../Profile/AccountAndProfile';
import { isMobile } from "react-device-detect";
import Header from './Header';
import ProductData from '../../../ProductData'
import { Link, useLocation, useHistory } from 'react-router-dom';



const Layout = () => {

    let location = useLocation();
    let { state } = location
    let profile = state?.profile;
    let basket = state?.basket;

    console.log(profile)
    console.log(basket)


    const { products } = ProductData;
    const [selectedTab, setSelectedTab] = useState('home-tab')
    const [fullScreen, setFullScreen] = useState(true)
    const [hidden, setHidden] = useState(false)
    const [display, setDisplay] = useState(true);
    const [basketItems, setBasketItems] = useGlobalState('basketItems');
    const [likedProducts, setLikedProducts] = useGlobalState('likedProducts');
    const [loginStatus, setLoginStatus] = useGlobalState('loginStatus');





    const findLikedProduct = () => {
        const liProduct = products.filter((item) => item.liked == true);
        setLikedProducts([...liProduct]);
    }

    const myComponent = () => {


        if (isMobile) {

            return (

                <div style={fullScreen ? { position: 'fixed', height: '100%', width: '100%', top: 0 } : { height: 400 }}>
                    <TabBar
                        unselectedTintColor="#949494"
                        tintColor="#33A3F4"
                        barTintColor="white"
                        hidden={hidden}
                    >

                        <TabBar.Item

                            key="home"
                            icon={<div style={{
                                width: '22px',
                                height: '22px',
                                background: 'url(./image/homeIcon.png) center center /  21px 21px no-repeat'
                            }}
                            />
                            }
                            selectedIcon={<div style={{
                                width: '22px',
                                height: '22px',
                                background: 'url(./image/homeActive.png) center center /  21px 21px no-repeat'

                            }}
                            />
                            }
                            selected={selectedTab === 'home-tab'}
                            onPress={() => {
                                setSelectedTab('home-tab')
                            }}
                            data-seed="logId"
                        >

                            <Home />
                            {/* {bottomtab()} */}
                        </TabBar.Item>

                        <TabBar.Item
                            icon={
                                <div style={{
                                    width: '22px',
                                    height: '22px',
                                    background: 'url(./image/searchIcon.png) center center /  21px 21px no-repeat'
                                }}
                                />
                            }
                            selectedIcon={
                                <div style={{
                                    width: '22px',
                                    height: '22px',
                                    background: 'url(./image/searchActive.png) center center /  21px 21px no-repeat'
                                }}
                                />
                            }

                            key="Search"
                            selected={selectedTab === 'search-tab'}
                            onPress={() => {
                                setSelectedTab('search-tab')
                            }}
                            data-seed="logId1"
                        >
                            <Search3 />
                        </TabBar.Item>

                        <TabBar.Item
                            icon={
                                <div style={{
                                    width: '22px',
                                    height: '22px',
                                    background: 'url(./image/basketIcon.png) center center /  21px 21px no-repeat'
                                }}
                                />
                            }
                            selectedIcon={
                                <div style={{
                                    width: '22px',
                                    height: '22px',
                                    background: 'url(./image/basketActive.png) center center /  21px 21px no-repeat'
                                }}
                                />
                            }

                            key="Search"
                            selected={selectedTab === 'busket-tab'}
                            badge={basketItems.length}
                            onPress={() => {
                                setSelectedTab('busket-tab')
                            }}
                            data-seed="logId1"
                        >



                            <Busket setSelectedTab={setSelectedTab} />

                            {/* <MyAddress /> */}

                        </TabBar.Item>

                        <TabBar.Item

                            icon={
                                <div style={{
                                    width: '22px',
                                    height: '22px',
                                    background: 'url(./image/favorite.png) center center /  21px 21px no-repeat'
                                }}
                                />
                            }
                            selectedIcon={
                                <div style={{
                                    width: '22px',
                                    height: '22px',
                                    background: 'url(./image/likedActive.png) center center /  21px 21px no-repeat'
                                }}
                                />
                            }
                            key="Search"
                            selected={selectedTab === 'liked-tab'}
                            onClick={() => findLikedProduct}
                            onPress={() => {
                                setSelectedTab('liked-tab')

                            }}
                            data-seed="logId1"
                        >
                            <Liked />
                        </TabBar.Item>

                        <TabBar.Item
                            icon={
                                <div style={{
                                    width: '22px',
                                    height: '22px',
                                    background: 'url(./image/profileIcon.png) center center /  21px 21px no-repeat'
                                }}
                                />
                            }
                            selectedIcon={
                                <div style={{
                                    width: '22px',
                                    height: '22px',
                                    background: 'url(./image/profileActive.png) center center /  21px 21px no-repeat'
                                }}
                                />
                            }

                            key="Search"
                            selected={selectedTab === 'profile-tab'}
                            onPress={() => {
                                setSelectedTab('profile-tab')
                            }}
                            data-seed="logId1"
                        >
                            {loginStatus ? <Profile /> : <Link to="/getStart"><div style={{ textAlign: "center", fontFamily: "Poppins", marginTop: "200px", background: "#2FDBB", color: "ffffff" }}> Please login  </div></Link>}

                        </TabBar.Item>

                    </TabBar>
                </div>

            )
        } else {

            return (

                <div>

                    <div style={{ position: "sticky", top: 0, zIndex: 9999 }}>
                        <Header numberOfOrder={basketItems.length} />

                    </div>
                    <Home />
                </div>
            )
        }
    }


    return (


        <div >

            {myComponent()}

        </div>










    );
};

export default Layout;