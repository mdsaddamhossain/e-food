import React from 'react';
import './layout.css'
import { Row, Col, Badge, Select, Space, Cascader } from 'antd';
import { SearchOutlined, ShoppingCartOutlined, HeartTwoTone } from '@ant-design/icons';
import { useGlobalState } from '../../../store'
import ProductData from '../../../ProductData'
import { Link } from 'react-router-dom';


const Header = (props) => {

    const { products } = ProductData;
    const [basketItems, setBasketItems] = useGlobalState('basketItems');
    const [searchTerm,setSearchTerm] = useGlobalState("searchTerm");
    const [currentUser, setCurrentUser] = useGlobalState('currentUser');

   console.log(currentUser,'heaer')
    

    return (
        <div>

            <Row>
                <Col xs={{ span: 24 }}>
                    <div className="header">

                    <p style={{color:"white",fontSize:"24px",cursor:"pointer",marginLeft:"50px",position:"absolute",marginTop:"20px"}}> &#9776; </p>
                       <div className="logo"> <Link to='/layout'> <img src="./image/foodlogo.jpg" width="50px" height="50px" style={{borderRadius:"30px"}} alt="error" /> </Link></div>
                        <div className="searchBox" >

                            <select className="drop-style" style={{color:"gray",fontFamily:"Poppins",}}>
                                <option>Catagory</option>
                                <option>Breakfast</option>
                                <option>Lunch</option>
                                <option>Dinner</option>
                            </select>


                            <input type="text" placeholder="Search food catagory" className="input-style"  onChange={(event)=>setSearchTerm(event.target.value)} />

                            <SearchOutlined style={{ flexGrow: 1, marginTop: "8px", cursor: "pointer", fontSize: "1.5rem",color:"white",width:"40px" }} />

                        </div>
                        <div className="cart">
                            <Link to="/liked"  ><HeartTwoTone twoToneColor="red"  style={{ fontSize: "2rem", marginLeft: "30px" }} /></Link> 
                            <Link to="/busket"> <ShoppingCartOutlined style={{ fontSize: "2rem", marginLeft: "20px",color:"white" }} /></Link>
                            <Badge  count={basketItems.length}  style={{ position: "absolute", top: "-10px", left: "30px",border:"none",background:"#F99928" }}></Badge>
                            

                        </div>
                        <div className="profile">
                            <div style={{ display: "flex", justifyContent: "center", flexDirection: "column" }}>
                              {currentUser.fullName ? <Link to="profile"> <img src={currentUser.image} className="image-styles" /> </Link> : <Link to="/getStart"><img src={currentUser.image} className="image-styles" /></Link>} 
                              {currentUser?.fullName ?  <span  className="toltipStyle"> {currentUser =={} ? "your name": currentUser.fullName } <br/> {currentUser == {} ? "your email" : currentUser.email} </span> : <span  className="toltipStyle"> Login </span>}  
                            </div>
                        </div>

                    </div>



                </Col>
            </Row>





        </div>
    );
};

export default Header;