import React, { useEffect, useState } from 'react';
import './CartAndBusket.css'
import { Row, Col, message } from 'antd';
import { Link } from 'react-router-dom'
import { useGlobalState } from '../../../store'
import { isMobile } from "react-device-detect";
import BasketDesign from './BasketDesign';
import Header from '../Layout/Header';




const Busket = (props) => {

    const [total, setTotal] = useGlobalState('total');
    const [basketItems, setBasketItems] = useGlobalState('basketItems');
    const [displayIncButton, setDisplayIncButton] = useState(false);

    let totals = 0;

    basketItems.forEach(element => {
        totals += element.subTotalPrice
    });


    // quantity increment and deccrement functions


    const onAdd = (product) => {

        const exist = basketItems.find((x) => x.id === product.id);

        if (exist) {
            setBasketItems(
                basketItems.map((x) =>
                    x.id === product.id ? { ...exist, quantity: exist.quantity + 1, subTotalPrice: exist.subTotalPrice + product.perUnitPrice } : x
                )
            );
        }
    }

    const onRemove = (product) => {

        const exist = basketItems.find((x) => x.id === product.id);

        if (exist) {
            setBasketItems(
                basketItems.map((x) =>
                    x.id === product.id ? { ...exist, quantity: exist.quantity - 1, subTotalPrice: exist.subTotalPrice - product.perUnitPrice } : x
                )
            );
        }
    }

    const onDelete = (id) => {
        const newBasketItems = basketItems.filter(item => item.id != id);
        
        setBasketItems(newBasketItems);
    }

    const testMobile = () => {
        if (isMobile) {

            return (
                <div className="common-top-margin">

                    {basketItems.length == 0 ?
                        <div >
                            <Row justify="center">
                                <Col >
                                    <img src="./image/cart-loader.gif" width='100%' />
                                    <p style={{ textAlign: "center" }}>card empty</p>
                                </Col>
                            </Row>

                        </div> : <BasketDesign basketItems={basketItems} onAdd={onAdd} onRemove={onRemove} onDelete={onDelete} setTotal={setTotal} totals={totals} />}


                </div>
            )
        } else {
            return (
                <div>
                    <div style={{ position: "sticky", top: 0, zIndex: 9999 }} >
                        <Header />
                    </div>
                    {basketItems.length == 0 ?
                        <div >
                            <Row justify="center" >
                                <Col >

                                    <img src="./image/cart-loader.gif" width='100%' />
                                    <Link to="/layout">  <div className="btn-design"> continue shoping  </div>  </Link>

                                </Col>
                            </Row>

                        </div> : <BasketDesign basketItems={basketItems} onAdd={onAdd} onRemove={onRemove} onDelete={onDelete} setTotal={setTotal} totals={totals} />}

                </div>
            )

        }
    }


    return (


        <div>

            {testMobile()}


        </div>
    );
};

export default Busket;