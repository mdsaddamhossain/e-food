import React, { useState, useEffect } from 'react';
import { Row, Col, Popconfirm, message } from 'antd';
import { Link } from 'react-router-dom'

export function Quantity({ onAdd, onRemove, item, index, onDelete }) {

    const [isOpen, setOpen] = useState(false);
    const [skLoading, setSkLoading] = useState(true);

    const confirm =(id)=> {
        
        message.success({
            content: 'successfully delete',
           
            style: {
              marginTop: '20vh',
             
            },
          });
        onDelete(id)

      }
      const cancel = (e) => {
      
        }

    useEffect(() => {
        setTimeout(() => {
            setSkLoading(false)
        }, 2000)
    })

    return (


        <Col key={index} xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} style={{ display: "flex", justifyContent: "space-between" }}>
            <div style={{ flexGrow: "2" }}>
                <img src={item.imgs} width="80px" height="80px" />
            </div>
            <div style={{ flexGrow: "10", marginTop: "2px" }}>
                <p className="name-style"> {item.name}</p>
                <p className="pric-style">$ {item.subTotalPrice}.00</p>
            </div>
            <div style={{ flexGrow: "4", textAlign: "right", marginTop: "2px" }}>
                <Popconfirm
                    title="Are you sure to delete this item?"
                    onConfirm={()=>confirm(item.id)}
                    onCancel={cancel}
                    okText="Delete"
                    cancelText="No"
                >
                    <p>  <img src="./image/deleteIcon.png" alt="erron" width="22px" height="22px" style={{ cursor: "pointer" }} /> </p>
                </Popconfirm>

                <div className="amount">  {item.quantity}<img src="./image/dropdownIcon.png" alt="erron" style={{ marginLeft: "10px", cursor: "pointer" }} onClick={() => {
                    //buttonDisplayHandaler(item.id)
                    setOpen(!isOpen)
                }} />
                    {isOpen ? <div>
                        <button style={{ cursor: "pointer", width: "30px", marginRight: "5px", background: "#2FDBBC", border: "none", color: "white", fontSize: "18px", fontFamily: 'Poppins', fontWeight: "700", borderRadius: "4px" }} onClick={() => onAdd(item)} > + </button>
                        <button style={{ cursor: "pointer", width: "30px", background: "red", border: "none", color: "white", fontSize: "18px", fontWeight: "700", fontFamily: 'Poppins', borderRadius: "4px" }} onClick={() => onRemove(item)} disabled={item.quantity == 1 ? true : false}> -</button>
                    </div> : ''}
                </div>

            </div>

        </Col>



    )


}