import React, { useState, useEffect } from 'react';
import { Row, Col, Spin, Skeleton, List } from 'antd';
import { Link } from 'react-router-dom';
import { Quantity } from './Quantity';
import { useHistory } from "react-router-dom";

const BasketDesign = ({ basketItems, onAdd, onRemove, setTotal, totals, onDelete }) => {

    const [loading, setLoading] = useState(false);
    const [skLoading, setSkLoading] = useState(true);
    let history = useHistory();

    const handalClick = () => {

        setLoading(true);
        setTimeout(() => {

            setLoading(false);
            setTotal(totals)
            history.push("/checkout");
        }, 2000)
    }

    useEffect(() => {
        setTimeout(() => {
            setSkLoading(false)
        }, 2000)
    })

    return (
        <div>



            <Row>
                <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }}>
                    <p className="basket">BASKET</p>





                    
                </Col>
                <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} style={{position:"relative"}}>
                   
                    <List
                        dataSource={basketItems}
                        renderItem={item => (
                            <List.Item key={item.id}>
                                <Skeleton loading={skLoading} avatar active >
    
                                    {/* <Quantity item={item} index onAdd={onAdd} onRemove={onRemove} onDelete={onDelete} /> */}

                                    
                                    

                                </Skeleton>
                            </List.Item>
                        )}

                    />


                </Col>

               

               
                {skLoading ? " " :  basketItems.map((item, index) => {
                        return (
                            <Quantity item={item} index onAdd={onAdd} onRemove={onRemove} onDelete={onDelete} />
                        )
                    })}
                    {/* {basketItems.map((item, index) => {
                        return (
                            <Quantity item={item} index onAdd={onAdd} onRemove={onRemove} onDelete={onDelete} />
                        )
                    })} */}


                    <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} style={{ position: "relative", marginTop: "10vmin" }} >
                        <p className="total-text">TOTAL</p>
                        <p className="total">$ {totals}.00</p>



                        <div className="register-button" onClick={handalClick}>
                            <Spin spinning={loading} style={{ marginTop: "20px", marginLeft: "30%" }}> </Spin>
                            proceed to checkout
                        </div>

                    </Col>

              

             

            </Row>

        </div>
    );
};

export default BasketDesign;