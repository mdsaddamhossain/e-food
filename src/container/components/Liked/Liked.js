import React, { useEffect, useState } from 'react';
import { Row, Col, Skeleton, List, Popconfirm, message } from 'antd';
import { StarOutlined, LikeOutlined, MessageOutlined } from '@ant-design/icons';
import { useGlobalState } from '../../../store'
import './Liked.css'
import { isMobile } from "react-device-detect";
import Header from '../Layout/Header'
import ProductData from '../../../ProductData'


const Liked = (props) => {

    const { products } = ProductData;
    const [likedProducts, setLikedProducts] = useGlobalState('likedProducts');
    const [skLoading, setSkLoading] = useState(true);
    const [loginStatus, setLoginStatus] = useGlobalState('loginStatus');

    const onDeleteItem = (id) => {
        const newLikeProduct = likedProducts.filter(item => item.id != id);
        setLikedProducts(newLikeProduct);
        message.success({
            content: 'successfully delete',

            style: {
                marginTop: '20vh',

            },
        });
    }

    const cancel = (e) => {

    }

    const browserTest = () => {
        if (isMobile) {
            return (
                null
            )
        } else {
            return (
                <Header />
            )
        }
    }
    useEffect(() => {
        setTimeout(() => {
            setSkLoading(false)
        }, 1000)
    })

    return (
        <div >
            {browserTest()}

            <Row>
                <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} className="common-top-margin">
                    <p className="basket">LIKED</p>
                    <List
                        dataSource={likedProducts}
                        renderItem={item => (
                            <List.Item
                                key={item.id}

                            >


                                <Skeleton loading={skLoading} active avatar>

                                    <div style={{ flexGrow: "2" }}>
                                        <img src={item.img} width="80px" height="80px" />
                                    </div>


                                    <div style={{ flexGrow: "10", marginTop: "2px" }}>
                                        <p className="name-style"> {item.name} </p>
                                        <p className="pric-style">$ {item.price}.00</p>
                                    </div>
                                    <div style={{ flexGrow: "4", textAlign: "right", marginTop: "2px" }}>
                                        <Popconfirm
                                            title="Are you sure to delete this item?"
                                            onConfirm={() => onDeleteItem(item.id)}
                                            onCancel={cancel}
                                            okText="Delete"
                                            cancelText="No"
                                        >
                                            <p>  <img src="./image/deleteIcon.png" alt="erron" style={{ width: "22px", height: "22px", cursor: "pointer" }} /> </p>
                                        </Popconfirm>


                                        <p className="amount"> <img src='./image/boxed.png' /> </p>
                                    </div>
                                </Skeleton>




                            </List.Item>
                        )}
                    />

                </Col>
            </Row>

        </div>
    );
};

export default Liked;