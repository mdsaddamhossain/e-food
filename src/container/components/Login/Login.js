import React, { useState } from 'react';
import { useGlobalState } from '../../../store'
import './Login.css';
import { Row, Col, Input, Modal, Spin, message } from 'antd';
import { Link } from 'react-router-dom';
import Register from '../Register/Register';
import ForgotPassword from './ForgotPassword';
import { useHistory } from "react-router-dom";
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';



const Login = (props) => {

    const loginSchema = Yup.object({
        email: Yup.string()
            .required('Email is required'),
        password: Yup.string()
            .required('Password is required')

    })

    const [modal, setModal] = useState(false);
    const [forgotModal, setForgotModal] = useState(false);
    var [checkInfo, setCheckInfo] = useState(false);
    const [loginStatus, setLoginStatus] = useGlobalState('loginStatus');
    const [customers, setCustomers] = useGlobalState('customers');
    const [currentUser, setCurrentUser] = useGlobalState('currentUser');
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");


    const [loading, setLoading] = useState(false);
    let history = useHistory();


    const handalChaneEmail = (e) => {
        setEmail(e.target.value)

    }
    const handalChanePassword = (e) => {
        setPassword(e.target.value)
    }
    console.log(customers, 'login')

    const handalClick = (values) => {
        checkInfo = customers.find(user => user.email == values.email && user.password == values.password);


        setLoading(true);

        setTimeout(() => {

            setLoading(false);

            if (props.checkOut) {
                if (checkInfo) {
                    setLoginStatus(true);
                   // localStorage.setItem("loginStatus",true)
                    setCurrentUser(checkInfo);
                   //localStorage.setItem("user",JSON.stringify(checkInfo))
                    message.success({
                        content: 'Login successfull',
                        style: {
                            marginTop: '20vh',
                        },
                    })
                    history.push('/checkOut');
                } else {
                    message.error({
                        content: 'password or email not match',

                        style: {
                            marginTop: '20vh',
                        },
                    })

                    return;
                }
            }

            else {
                if (checkInfo) {
                    setLoginStatus(true);
                   // localStorage.setItem("loginStatus",true)
                    setCurrentUser(checkInfo);
                   //localStorage.setItem("user",JSON.stringify(checkInfo))
                    message.success({
                        content: 'Login successfull',

                        style: {
                            marginTop: '20vh',

                        },
                    })
                    history.push('/layout')
                } else {
                    message.error({
                        content: 'password or email not match',

                        style: {
                            marginTop: '20vh',

                        },
                    })
                    return;
                }
            }


        }, 2000)
    }

    const showModal = () => {
        setModal(true);
        props.modalHidden();
    }

    const hideModal = () => {
        setModal(false);

    }

    const showForgotModal = () => {
        setForgotModal(true);
        props.modalHidden()
    }

    const hideForgotModal = () => {
        setForgotModal(false);
    }

    return (
        <div className="common-top-margin">

            <Row>
                <Col xs={{ offset: 2, span: 20 }}>
                    <img src="./image/vector.png" onClick={props.modalHidden} style={{ cursor: "pointer" }} />
                </Col>
                <Col xs={{ offset: 2, span: 20 }} >
                    <p className="reg-title">LOGIN</p>
                </Col>

                <Formik
                    initialValues={{
                        email: '',
                        password: ''
                    }}
                    validationSchema={loginSchema}

                    onSubmit={values => {
                        // same shape as initial values
                        handalClick(values)
                        
                      }}
                >
                    {() => (
                        <div style={{ width: "100%", margin: "10px 25px" }}>

                            <Form>
                                <p className="common-text">Email</p>
                                <Field name="email" type="email" className="inputFieldStyle" />
                                <ErrorMessage name="email" >
                                    {msg => <div style={{ color: 'red', marginLeft: "20px", marginTop: "5px", fontFamily: "Poppins", position: "relative", top: "-15px" }}>{msg}</div>}
                                </ErrorMessage>

                                <p className="common-text">Password</p>
                                <Field name="password" className="inputFieldStyle" type="password" />
                                <ErrorMessage name="password" >
                                    {msg => <div style={{ color: 'red', marginLeft: "20px", marginTop: "5px", fontFamily: "Poppins", position: "relative", top: "-15px" }}>{msg}</div>}
                                </ErrorMessage>

                                <button className="register-button" type="submit" style={{ fontFamily: 'Bebas Neue', marginTop: "100px" }} >
                                    <Spin spinning={loading} style={{ marginTop: "20px", marginLeft: "30%" }}> </Spin>
                                    LOGIN
                                </button>


                            </Form>
                        </div>
                    )}


                </Formik>


                <Col xs={{ offset: 2, span: 20 }} style={{ marginTop: "30px", textAlign: "center" }} >
                    <p className="question">Do't have an account ? <spam style={{ color: "#F99928", cursor: "pointer" }} onClick={showModal}> Register or</spam> <Link to="/layout"><span style={{ color: "#2FDBBC", cursor: "pointer" }}>Skip Now!</span></Link> </p>
                </Col>

            </Row>

            {/* register modal */}

            <Modal
                visible={modal}

                okButtonProps={{
                    style: {
                        display: "none",
                    },
                }}
                cancelButtonProps={{
                    style: {
                        display: "none",
                    },
                }}
                closable={false}

            >
                <Register modalHidden={hideModal} />


            </Modal>
            {/* forgot password modal */}

            <Modal
                style={{ height: "600px" }}
                visible={forgotModal}
                okButtonProps={{
                    style: {
                        display: "none",
                    },
                }}
                cancelButtonProps={{
                    style: {
                        display: "none",
                    },
                }}
                closable={false}

            >
                <ForgotPassword modalHidden={hideForgotModal} />


            </Modal>


        </div>
    );
};

export default Login;