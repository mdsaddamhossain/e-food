import React, { useState, useEffect } from 'react';
import './BootmTab.css'
import { useGlobalState } from '../../../store'
import { Row, Col, Card, message, Space, Skeleton, List, Avatar } from 'antd';
import ProductData from '../../../ProductData'
import CompanyData from '../../../CompanyData.json'
import { Link, useHistory } from 'react-router-dom';
import { isMobile } from "react-device-detect";





const { Meta } = Card;



const Home = (props) => {

    const { products } = ProductData;
    const [basketItems, setBasketItems] = useGlobalState('basketItems');
    const [searchTerm, setSearchTerm] = useGlobalState('searchTerm');
    const [likedProducts, setLikedProducts] = useGlobalState('likedProducts');
    const [currentUser, setCurrentUser] = useGlobalState('currentUser');
    const [skLoading, setSkLoading] = useState(true);



    const onUpdateLikedItem = (data) => {
        const exist = likedProducts.find((x) => x.id === data.id);
        if (exist) {
            const newLikeProduct = likedProducts.filter(item => item.id != data.id);
            setLikedProducts(newLikeProduct);
            message.warning({
                content: 'Remove from liked',

                style: {
                    marginTop: '20vh',

                },
            })
        } else {

            setLikedProducts([...likedProducts, data])
            message.success({
                content: 'Liked added successfull',

                style: {
                    marginTop: '20vh',

                },
            })
        }

    }


    const history = useHistory();

    const handaleClick = (data) => {

        const existProduct = basketItems.find(x => x.id == data.id);
        if (existProduct) {

            message.warning({
                content: 'This Product already added',
                style: {
                    marginTop: '20vh',
                },
            })
            return;
        } else {

            history.push({
                pathname: '/product',
                state: data
            });
        }

    }


    useEffect(() => {
        setTimeout(() => {
            setSkLoading(false)
        }, 2000)
    })


    return (
        <div style={{ background: "#F7F7F7", paddingTop: "15px" }}>

            <Row>
                <Col xs={{ offset: 2, span: 20 }} >
                    <div className="hello-style">

                        <p className="jhone">Hello!<span style={{ color: "#2FDBBC" }}> {currentUser.fullName} </span> </p>
                        <div style={{ display: "flex", marginTop: "2px" }}>
                            <p className="home">Home</p>
                            <img src="./image/vector2.png" className="location-icon" />
                        </div>
                    </div>
                </Col>

                <Col xs={{ offset: 2, span: 20 }} >
                    <div className="get-discount">

                        <Row>
                            <Col offset={2} span={20} style={{ paddingTop: "15px" }}>
                                <p className="discount" >GET  <span style={{ color: "#2D2D2D", lineHeight: "43px", fontSize: "36px" }}> 50%</span> AS A JOINING BONUS</p>
                            </Col>

                            <Col offset={2} span={20} >

                                <Row>
                                    <Col style={{ width: "45%", marginTop: "10px", marginRight: "5%" }}>
                                        <p className="use-code" >USE CODE OF CHECKOUT </p>
                                    </Col>
                                    <Col style={{ width: "50%", marginTop: "10px", textAlign: "right" }}>
                                        <img src="./image/hand.png" width="200px" height="100px" style={{ position: "relative", right: "0", top: "-50px", left: "-20px" }} />
                                    </Col>
                                    <Col style={{ width: "45%", marginTop: "-70px", marginRight: "5%" }}>
                                        <div >
                                            <p style={{ color: "#2D2D2D", lineHeight: "43px", fontSize: "36px", marginTop: "-20px" }} >NEW50 </p>
                                        </div>
                                    </Col>
                                </Row>

                            </Col>

                        </Row>

                    </div>
                </Col>

                <Col xs={{ offset: 2, span: 20 }} style={{ marginTop: "40px" }}>
                    <div className="reco-style">
                        <p>RECOMMENDED FOR YOU</p>
                    </div>
                </Col>

                <Col xs={{ offset: 2, span: 20 }} style={{ marginTop: "20px" }} >

                    <Row >

                        <Skeleton active paragraph={{ rows: 20 }} loading={skLoading} Avatar>





                            {products.filter((item, key) => {
                                if (searchTerm == "") {
                                    return item
                                } else if (item.type.toLocaleLowerCase().includes(searchTerm.toLocaleLowerCase())) {
                                    return item;
                                }
                            }).map((product, key) => {

                                return (

                                    <Col xs={{ span: 12 }} sm={8} md={8} lg={6} style={{ marginTop: "20px", padding: 10 }}>

                                        <Card
                                            onClick={() => handaleClick(product)}
                                            hoverable
                                            style={{ width: "100%", borderRadius: "10px", }}
                                            cover={<img alt="example" src={product.img} height={isMobile ? "150px" : "350px"} style={{ borderRadius: "10px" }} />}

                                        >
                                            <div className={product.liked ? "loveVector" : "loveVectorUnlike"}> <img src="./image/love.png" style={{ marginTop: "5px" }} onClick={() => onUpdateLikedItem(product)} /></div>



                                            <Meta title={product.name} style={{ position: "relative", top: "-15px", left: "-10px", fontFamily: "Poppins", color: "#2D2D2D", fontSize: "14px", lineHeight: "21px" }} />

                                            <div style={{ display: "flex", justifyContent: "space-between", marginTop: "10px" }}>
                                                <p className="price-style">${product.price}</p>
                                                <img src={product.subImage} style={{ width: "22px", height: "22px", marginRight: "-10px" }} />
                                            </div>
                                        </Card>

                                    </Col>

                                )
                            })}

                        </Skeleton>


                    </Row>



                </Col>


                <Col xs={{ offset: 2, span: 20 }} style={{ marginTop: "25px" }}>
                    <div className="reco-style">
                        <p>RESTAURANTS</p>

                    </div>
                </Col>

                <Col xs={{ offset: 2, span: 20 }} style={{ marginTop: "2px" }}>
                    <Row>
                        {
                            CompanyData.map((com, index) => {
                                return (
                                    <Col key={index} xs={{ span: 6 }} style={{ width: "100%", marginTop: " 20px" }}>
                                        <div style={{ width: "80px", height: "80px", borderRadius: "20px", background: "white", display: "flex", justifyContent: "center", alignSelf: "center" }}>
                                            <img src={com.logo} />
                                        </div>

                                    </Col>

                                )
                            })
                        }
                    </Row>
                </Col>
                <Col xs={{ offset: 2, span: 20 }} style={{ marginTop: "25px" }}>
                    <div className="reco-style">
                        <p>POPULAR IN YOUR AREA</p>
                    </div>
                </Col>


            </Row>
        </div>
    );
};

export default Home;