import React, { useState } from 'react';
import { useGlobalState } from '../../../store'
import './Register.css'
import { Row, Col, Input, Modal, message, Spin } from 'antd';
import Login from '../Login/Login';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';



const Register = (props) => {

    const SignupSchema = Yup.object().shape({
        fullName: Yup.string()
            .min(4, 'Too Short! minimun 4 character')
            .max(30, 'Too Long!')
            .required('Required'),
        email: Yup.string()
            .email('Invalid email')
            .required('Required'),
        phone: Yup.string()
            .min(8, 'Must be 8 character or more')
            .required('Required'),
        password: Yup.string()
            .min(6, 'Must be 6 character or more')
            .required('Required'),
    });

    const [modal, setModal] = useState(false);
    const [customers, setCustomers] = useGlobalState('customers');
    const [user, setUser] = useState({ fullName: "", phone: "", email: "", password: "" })
    const [loading, setLoading] = useState(false);


    
    console.log(customers,'register')

    const showModal = (values) => {
        
        const exist = customers.find((customer) => customer.email ===  values.email);
         if (exist) {
            message.warning({
                content: 'Alredy Register this Email!',

                style: {
                    marginTop: '20vh',

                },
            })
            return
        }
        else {
            setLoading(true)
            setTimeout(() => {
                setLoading(false);

                setCustomers([...customers, values])
                message.success({
                    content: 'Registration successfull',

                    style: {
                        marginTop: '20vh',

                    },
                })
                setModal(true);
                props.modalHidden();

            }, 2000)
        }
    }

    const loginModal = () => {
        setModal(true);
        props.modalHidden();
    }

    const hideModal = () => {
        setModal(false);
    }

    const name = <p> Jhon Doe </p>

    return (
        <div className="common-top-margin">

            <Row>
                <Col xs={{ offset: 2, span: 20 }} >
                    <img src="./image/vector.png" style={{ cursor: "pointer" }} onClick={props.modalHidden} />
                </Col>
                <Col xs={{ offset: 2, span: 20 }}  >
                    <p className="reg-title">REGISTER</p>
                </Col>
            </Row>
            <Formik
                initialValues={{
                    fullName: '',
                    email: '',
                    phone: '',
                    password: '',
                }}
                validationSchema={SignupSchema}
                onSubmit={values => {
                    // same shape as initial values
                    showModal(values)
                    
                  }}
                
            >
                {({ errors, touched }) => (
                    <Form style={{ margin: "10px 25px" }}>



                        <p className="common-text">Full Name</p>
                        <Field name="fullName" className="inputFieldStyle" />
                        <ErrorMessage name="fullName" >
                            {msg => <div style={{ color: 'red', marginLeft: "20px", marginTop: "5px", fontFamily:"Poppins",position:"relative",top:"-15px" }}>{msg}</div>}
                        </ErrorMessage>

                        <p className="common-text">Email</p>
                        <Field name="email" type="email" className="inputFieldStyle" />
                        <ErrorMessage name="email" >
                            {msg => <div style={{ color: 'red', marginLeft: "20px", marginTop: "5px", fontFamily:"Poppins",position:"relative",top:"-15px" }}>{msg}</div>}
                        </ErrorMessage>

                        <p className="common-text">Phone</p>
                        <Field name="phone" className="inputFieldStyle" />
                        <ErrorMessage name="phone" >
                            {msg => <div style={{ color: 'red', marginLeft: "20px", marginTop: "5px", fontFamily:"Poppins",position:"relative",top:"-15px" }}>{msg}</div>}
                        </ErrorMessage>

                        <p className="common-text">Password</p>
                        <Field name="password" className="inputFieldStyle" type="password" />
                        <ErrorMessage name="password" >
                            {msg => <div style={{ color: 'red', marginLeft: "20px", marginTop: "5px", fontFamily:"Poppins",position:"relative",top:"-15px" }}>{msg}</div>}
                        </ErrorMessage>

                        <p></p>

                        <button className="register-button" type="submit"  >
                        <Spin spinning={loading} style={{marginTop:"20px", marginLeft: "30%" }}> </Spin>
                            Register
                         </button>


                    </Form>
                )}



            </Formik>

           
            <Row>
                <Col xs={{ offset: 2, span: 20 }} style={{  textAlign: "center" }} >
                    <p className="question">Already have an account? <spam style={{ color: "#F99928", cursor: "pointer" }} onClick={loginModal}> Login</spam></p>
                </Col>

            </Row>


            <Modal
                visible={modal}

                okButtonProps={{
                    style: {
                        display: "none",
                    },
                }}
                cancelButtonProps={{
                    style: {
                        display: "none",
                    },
                }}
                closable={false}

            >
                <Login modalHidden={hideModal} checkOut={props.checkOut} />


            </Modal>

        </div>
    );
};

export default Register;