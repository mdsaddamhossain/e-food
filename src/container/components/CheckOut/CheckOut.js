import React, { useState } from 'react';
import { useGlobalState } from '../../../store'
import './CheckOut.css';
import { Row, Col, Button, Spin, Progress, message } from 'antd';
import { Link, useHistory } from 'react-router-dom';
import { isMobile } from "react-device-detect";
import Header from '../Layout/Header';
import styled from 'styled-components'
import * as moment from 'moment'




const CheckOut = (props) => {

    const [total, setTotal] = useGlobalState('total');
    const [numberOfCard, setNumberOfCard] = useGlobalState('numberOfCard');
    const [basketItems, setBasketItems] = useGlobalState('basketItems');
    const [loginStatus, setLoginStatus] = useGlobalState('loginStatus');
    const [myOrder, setMyOrder] = useGlobalState('myOrder');
    const [loading, setLoading] = useState(false);

    let history = useHistory();
    let m = moment();        
    let orderDat = m.format('D MMM YYYY');

    let date = new Date();
    let orderTime = date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()
    
    console.log(orderTime)
   

   
    const handalClick = () => {
        setLoading(true);

        setTimeout(() => {

            setLoading(false);
           
            //  history.push(loginStatus ? isSuccess ? confirmOrder() : wrongOrder() : backToLogin())

            if (loginStatus) {
                if (basketItems.length > 0) {

                   
                    setMyOrder([...myOrder,{totalPrice:total,products:basketItems,orderDate:orderDat,orderTime:orderTime}])
                    history.push("/orderConfirm")
                    setBasketItems([])

                } else {
                    history.push("/orderWrong")
                }

            } else {
                message.warning({
                    content: 'For transaction Please login first',
                    style: {
                        marginTop: '20vh',
                        color: "#2FDBBC",
                        fontFamily: "Poppins"
                    },
                })
                history.push({
                    pathname: '/getStart',
                    state: {
                        checkOut: 'checkOut'
                    }
                });
            }

        }, 2000)

    }



    const MyButton = styled(Button)`

            width: 100%;
            height: 40px;
            background-color: #2FDBBC;
            border-radius: 20px;
            text-align: center;
            line-height: 36px;
            color: #ffffff;
            font-weight: 400;
            font-size: 18px;
            text-transform: uppercase;
            cursor : pointer;
            border: none;

            &:hover{
                background-color: #2FDBBC;
                color:#F99928;
            }
        
    `

    const browserTest = () => {
        if (isMobile) {
            return (
                null
            )
        } else {
            return (
                <Header />
            )

        }
    }

    const backBatton = ()=>{
        if(isMobile){
            history.push({
                pathname : '/layout',
                state:{
                    basket : 'basket'
                }
            })
        }else{
            history.push('/busket')
        }
    }

    return (
        <div >

            {browserTest()}

            <Row>
                <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} className="common-top-margin">
                   <img src="./image/vector8.png" alt="error" style={{ cursor: "pointer" }} onClick={backBatton} />
                </Col>
                <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} style={{ marginTop: "40px" }}>
                    <p className="checkout">CHECKOUT</p>
                </Col>
                <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} style={{ marginTop: "150px" }}>
                    <p className="price">PRICE</p>
                </Col>
                <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} >
                    <p className="dollar-style">$ {total}.00</p>
                </Col>

                <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} style={{ marginTop: "20px" }}>
                    <p className="price">DELIVER TO</p>
                </Col>

                <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} style={{ marginTop: "", display: "flex", justifyContent: "space-between" }}>
                    <p className="home-style"> HOME </p>
                    <Link to="myAddress"> <p className="change" > Change </p></Link>
                </Col>

                <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} style={{ marginTop: "40px" }}>
                    <p className="price">PAYMENT METHOD</p>
                </Col>

                <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} style={{ marginTop: "", display: "flex", justifyContent: "space-between" }}>
                    <p className="home-style"> XXXX XXXX XXXX 2538 </p>
                    <Link to="paymentMethod">  <p className="change"> Change </p></Link>
                </Col>

                <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} style={{ marginTop: "30px" }}>
                    <MyButton onClick={handalClick}>
                        <Spin spinning={loading} style={{ marginTop: "20px", marginLeft: "40%" }}> </Spin>
                        CHECKOUT ORDER
                    </MyButton>

                    {/* </Link> */}
                </Col>
            </Row>

        </div>
    );
};

export default CheckOut;