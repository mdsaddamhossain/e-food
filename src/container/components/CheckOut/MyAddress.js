import React from 'react';
import { useGlobalState } from '../../../store'
import './CheckOut.css'
import { Row, Col } from 'antd';
import { Link,useLocation ,useHistory} from 'react-router-dom';
import { isMobile } from "react-device-detect";
import Header from './../Layout/Header';

const MyAddress = () => {

    const [currentUser, setCurrentUser] = useGlobalState('currentUser');
    let history = useHistory();
    let location = useLocation();
    let { state } = location
    let manageAddress = state?.manageAddr
    let addNewAddres = state?.addNewAddres
    

    const backButton = ()=>{
        if(manageAddress || addNewAddres){
            if(isMobile){
                history.push("/layout")
            }else{
                history.push('/profile')
            }
        }else{
            history.push("/checkOut")
        }
    }
    const handalClick = ()=>{
        if(manageAddress){
            history.push("/addNewAddress",{hint : "pro"} )
        }else{
            history.push("/addNewAddress")
        }
    }
    const browserTest = () =>{
        if(isMobile){
            return(
               null
            )
        }else{
            return(
               <Header />
            )    
        }
    }
    return (
        <div >

            {browserTest()}

            <Row>
                <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} className="common-top-margin">
                     <img src="./image/vector8.png" onClick={backButton} style={{ cursor: "pointer" }} />
                    <p className="my-address" style={{ marginTop: "50px" }}>My Address</p>
                    <p className="party-place" style={{ marginTop: "180px", }}>PARTY PLACE</p>
                    <p className="apt-style" >Apt. 12, Watson Bldg., 13th Ave. and St. James St., 406035</p>
                    <p className="office" style={{ marginTop: "50px" }}>OFFICE</p>
                    <p className="suit"> {currentUser?.address_office}</p>
                    <p className="office" style={{ marginTop: "50px" }}>Home</p>
                    <p className="suit"> {currentUser?.address_home} </p>

                   
                        <div className="add-new-address">
                           <p  onClick={handalClick}>ADD NEW ADDRESS </p> 
                        </div>
                  
                </Col>
            </Row>

        </div>
    );
};

export default MyAddress;