import React, { useState, useEffect } from 'react';
import { useGlobalState } from '../../../store'
import './CheckOut.css'
import { Row, Col, Progress, Skeleton } from 'antd';
import Header from '../Layout/Header';
import { isMobile } from "react-device-detect";
import { useHistory,useLocation } from 'react-router-dom';


const OrderConfirmed = () => {

    const [myOrder, setMyOrder] = useGlobalState('myOrder');
    let history = useHistory();

    const handaleClick = () =>{
        history.push({
            pathname: '/orderHistory',
            
        });
    }

    const browserTest = () => {
        if (isMobile) {
            return (
                null
            )
        } else {
            return (
                <Header />
            )
        }
    }


    return (
        <div>


            {browserTest()}
            

                <div style={{ background: "#4FE178", height: "100vh" }}>

                    <div >
                        <Row>
                            <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} style={{ marginTop: "120px", textAlign: "center" }}>
                          
                            <Progress type="circle" showInfo={true}  strokeColor="#ffffff" percent={100}/>

                                <p className="order-confirm" >ORDER CONFIRMED!</p>
                                <img src="./image/orderConfirm.png" />
                                <p className="hang-on">Hang on Tight! We've received your arder and we'll bring it to you ASAP!</p>
                                
                                <div className="order-track" onClick={handaleClick}>
                                    <p>TRACK MY ORDER</p>
                                </div>
                            </Col>
                        </Row>

                    </div>
                </div>

          
        </div>
    );
};

export default OrderConfirmed;