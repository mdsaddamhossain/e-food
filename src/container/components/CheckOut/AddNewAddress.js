import React from 'react';
import './CheckOut.css'
import { Row, Col, Input } from 'antd';
import { Link,useLocation,useHistory } from 'react-router-dom';
import { isMobile } from "react-device-detect";
import Header from './../Layout/Header';

const AddNewAddress = () => {

    let history = useHistory();
    let location = useLocation();
    const { state } = location
    const manageAddress = state?.hint
    console.log(manageAddress,"hint")
    const backButton = ()=>{
        if(manageAddress){
            history.push({
                pathname : "/myAddress",
                state :{
                    addNewAddres : "addNewAddres"
                }
            })
        }else{
            history.push("/myAddress")
        }
    }

    const browserTest = () =>{
        if(isMobile){
            return(
               null
            )
        }else{
            return(
               <Header />
            )    
        }
    }

    return (
        <div >
            {browserTest()}
            <Row>
                <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} className="common-top-margin">
                    <img src="./image/vector8.png" style={{ cursor:"pointer" }} onClick={backButton} />
                    <p className="my-address" style={{ marginTop: "50px" }}>ADD NEW ADDRESS</p>
                </Col>

                <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} style={{ marginTop: "100px" }}>
                    <p className="common-text">ADDRESS LINE1</p>
                </Col>
                <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} style={{ marginTop: "-35px" }} >
                    <div className="common-input">
                        <Input placeholder="Adress" style={{ width: "100%", height: "40px", borderRadius: "20px", background: "none", paddingLeft: "20px", fontFamily: "Poppins" }} />
                    </div>
                </Col>

                <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} style={{ marginTop: "20px" }}>
                    <p className="common-text">ADDRESS LINE2</p>
                </Col>
                <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} style={{ marginTop: "-35px" }} >
                    <div className="common-input">
                        <Input placeholder="Adress" style={{ width: "100%", height: "40px", borderRadius: "20px", background: "none", paddingLeft: "20px", fontFamily: "Poppins" }} />
                    </div>
                </Col>

                <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} style={{ marginTop: "50px" }} >
                    <Row>
                        <Col style={{ width: "45%", marginTop: "-30px", marginRight: "5%" }}>
                            <p className="common-text">ZIP CODE</p>
                        </Col>
                        <Col style={{ width: "45%", marginTop: "-30px", marginLeft: "5%" }}>
                            <p className="common-text">CITY</p>
                        </Col>
                        <Col style={{ width: "45%", marginTop: "-30px", marginRight: "5%" }}>
                            <div className="common-input">
                                <Input placeholder="000-000" style={{ width: "100%", height: "40px", borderRadius: "20px", background: "none", paddingLeft: "20px", fontFamily: "Poppins" }} />
                            </div>
                        </Col>
                        <Col style={{ width: "45%", marginTop: "-30px", marginLeft: "5%" }}>
                            <div className="common-input">
                                <Input placeholder="City" style={{ width: "100%", height: "40px", borderRadius: "20px", background: "none", paddingLeft: "20px", fontFamily: "Poppins" }} />
                            </div>
                        </Col>
                    </Row>
                </Col>

                <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} style={{ marginTop: "20px" }}>
                    <p className="common-text">COUNTRY</p>
                </Col>
                <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} style={{ marginTop: "-35px" }} >
                    <div className="common-input">
                        <Input placeholder="Country" style={{ width: "100%", height: "40px", borderRadius: "20px", background: "none", paddingLeft: "20px", fontFamily: "Poppins" }} />
                    </div>
                </Col>
                <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }}>
                    
                        <div className="add-new-address" onClick={ backButton}>
                            ADD NEW ADDRESS
                        </div>
                   
                </Col>
            </Row>

        </div>
    );
};

export default AddNewAddress;