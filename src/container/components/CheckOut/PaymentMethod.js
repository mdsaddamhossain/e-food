import React from 'react';
import { useGlobalState } from '../../../store'
import './CheckOut.css'
import { Row, Col, Input } from 'antd';
import { Link,useHistory,useLocation } from 'react-router-dom';
import { isMobile } from "react-device-detect";
import Header from './../Layout/Header';

const PaymentMethod = () => {

    const [currentUser, setCurrentUser] = useGlobalState('currentUser');
    let history = useHistory();
    let location = useLocation();
    const { state } = location
    let managePayment = state?.managePay
    let proPayment = state?.proPayment


    const backButton = ()=>{
        if(managePayment || proPayment){
            if(isMobile){
                history.push("/layout")
            }else{
                history.push('/profile')
            }
            
        }else{
            history.push("/checkOut")
        }
    }

    const handalClick = ()=>{
        if(managePayment){
            history.push("/addNewCard",{hint : "pro"} )
        }else{
            history.push("/addNewCard")
        }
    }

    const browserTest = () =>{
        if(isMobile){
            return(
               null
            )
        }else{
            return(
               <Header />
            )    
        }
    }

    return (
        <div >
            {browserTest()}
            <Row>
                <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} className="common-top-margin">
                     <img src="./image/vector8.png" onClick={backButton} style={{ cursor: "pointer" }} /> 
                    <p className="my-address" style={{ marginTop: "50px" }}>My PAYMENT METHOD</p>
                    <p className="office" style={{ marginTop: "180px", }}>CASH</p>
                    <p className="suit" >Pay Using Cash</p>
                    <p className="office" style={{ marginTop: "50px", }}>MASTERCARD - 0164</p>
                    <div style={{ display: "flex", justifyContent: "space-between" }}>
                        <p className="suit">{currentUser?.masterCard}</p>
                        <p className="suit">{currentUser?.masterCard ? currentUser.masterCard?.toString().split("").length : 0}/21</p>
                    </div>

                    <p className="party-place" style={{ marginTop: "50px", }}>VISA - 3648</p>
                    <div style={{ display: "flex", justifyContent: "space-between" }}>
                        <p className="apt-style">{currentUser?.visaCard}</p>
                        <p className="apt-style">{currentUser?.visaCard ? currentUser.visaCard?.toString().split("").length : 0}/23</p>
                    </div>
                   
                        <div className="add-new-address" onClick={handalClick}>
                            ADD NEW PAYMENT METHOD
                        </div>
                    
                </Col>
            </Row>

        </div>
    );
};

export default PaymentMethod;