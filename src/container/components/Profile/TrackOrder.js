import React from 'react';
import { useGlobalState } from '../../../store'
import './Profile.css';
import { Row, Col, Input } from 'antd';
import { Link } from 'react-router-dom'
import { isMobile } from "react-device-detect";
import Header from '../Layout/Header';

const TrackOrder = (props) => {


    const [myOrder, setMyOrder] = useGlobalState('myOrder');
    const mayOrderDetails = props.location.state || [];

    console.log(mayOrderDetails, '//////////')

    const browserTest = () => {
        if (isMobile) {
            return (
                null
            )
        } else {
            return (
                <Header />
            )
        }
    }

    return (
        <div >
            {browserTest()}
            <Row>
                <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} className="common-top-margin">
                    <Link to="/orderHistory"> <img src="./image/vector8.png" /> </Link>
                </Col>
                <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} style={{ marginTop: "50px" }}>
                    <p className="acc-and-pro">{mayOrderDetails[0]?.orderDate} </p>
                    <p className="price-total">$ {mayOrderDetails[0]?.totalPrice}.00</p>
                </Col>


                {mayOrderDetails[0]?.products.map((item, key) => {
                    return (
                        
                            
                            <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} style={{ marginTop: "50px" }}>
                                <div key={key}>
                                    <p className="egg-salad">{item.name}</p>
                                    <div style={{ display: "flex", justifyContent: "space-between" }}>
                                        <p className="pises">{item.quantity} PICES</p>
                                        <p className="pises"> $ {item.subTotalPrice}.00</p>
                                    </div>

                                </div>
                            </Col>
                      
                    )
                })}
            </Row>

        </div>
    );
};

export default TrackOrder;