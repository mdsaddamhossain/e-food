import React from 'react';
import './Profile.css';
import { Row, Col } from 'antd';
import ConditionData from '../../../ConditionData.json'
import { Link } from 'react-router-dom';
import { isMobile } from "react-device-detect";
import Header from '../Layout/Header';

const TermAndCondition = () => {

    const browserTest = () =>{
        if(isMobile){
            return(
               null
         )
        }else{
            return(
               <Header />
            )  
        }
    }

    return (
        <div >
            {browserTest()}
            <Row>
                <Col xs={{offset:2,span:20}} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} className="common-top-margin">
                  <Link to={ isMobile ? "/layout" : "/profile"} > <img src="./image/vector8.png" /></Link>
                </Col>
                <Col xs={{offset:2,span:20}} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} style={{ marginTop: "50px" }}>
                    <p className="acc-and-pro">TERMS AND CONDITIONS </p>

                </Col>

                <Col xs={{offset:2,span:20}} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} style={{ marginTop: "50px" }}>

                    {ConditionData.map((data, index) => {
                        return (
                            <div>
                                <p className="title-styling">{data.title} </p>
                                <p className="descrip-styling">{data.description}</p>
                            </div>
                        )
                    })}

                </Col>
            </Row>
        </div>
    );
};

export default TermAndCondition;