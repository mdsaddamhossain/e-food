import React from 'react';
import { Row, Col } from 'antd';
import { Link } from 'react-router-dom';

const LogoutModalStyle = (props) => {
    return (
        <div>
            <Row>
                <Col xs={{ offset: 2, span: 20 }} style={{marginTop:"50px"}}>
                    <img src="./image/vector.png" onClick={props.modalHidden} />
                </Col>
                <Col xs={{ offset: 2, span: 20 }} style={{marginTop:"100px"}} >
                    <p > Are you sure to logOut?</p>
                </Col>

                <Col xs={{ offset: 2, span: 20 }} style={{ marginTop: "200px" }} >
                    <Link to="/getStart" >
                        <div className="register-button">
                            LOGOUT
                        </div>
                    </Link>
                </Col>
                
                <Col xs={{ offset: 2, span: 20 }} style={{ marginTop: "20px" }} >
                    
                        <div className="calcelButton" onClick={props.modalHidden}>
                            CANCEL
                        </div>
                   
                </Col>


            </Row>

        </div>
    );
};

export default LogoutModalStyle;