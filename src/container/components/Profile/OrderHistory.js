import React from 'react';
import { useGlobalState } from '../../../store'
import './Profile.css';
import { Row, Col, Input } from 'antd';
import { Link,useHistory } from 'react-router-dom'
import { isMobile } from "react-device-detect";
import Header from '../Layout/Header';


const OrderHistory = () => {

    const [myOrder, setMyOrder] = useGlobalState('myOrder');

    let history = useHistory();
    
    const handalClicked = (data) =>{
        history.push({
            pathname : "/trackOrder",
            state : [data]
        })
    }

    console.log(myOrder, '////')

    const browserTest = () => {

        if (isMobile) {
            return (
                null
            )
        } else {
            return (
                <Header />
            )
        }
    }

    return (
        <div >
            {browserTest()}
            <Row>
                <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} className="common-top-margin">
                    <Link to={isMobile ? "/layout" : "/profile"} ><img src="./image/vector8.png" /></Link>
                </Col>
                <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} style={{ marginTop: "50px" }}>
                    <p className="acc-and-pro">ORDER HISTORY</p>
                </Col>


                {/* <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} style={{ marginTop: "30px", display: "flex", justifyContent: "space-between" }}>
                    <p className="date-style">{myOrder[0]?.orderDate}</p>
                    <p className="date-style">$ {myOrder[0]?.totalPrice}.00</p>
                </Col>

                <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} style={{ marginTop: "10px" }}>
                    <p className="items-order">{myOrder[0]?.products.length} items</p>
                </Col>
                <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} style={{ marginTop: "5px" }}>

                    <Link to="trackOrder">
                        <div className="checkOut">
                            TRACK ORDER
                        </div>
                    </Link>
                </Col> */}


                {
                    myOrder?.map((item,key)=>{
                        return(
                            <div style={{ width: "100%" }}>
                            <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} style={{ marginTop: "50px", display: "flex", justifyContent: "space-between" }}>
                                <p className="date-style">{item.orderDate+' ' +item.orderTime }</p>
                                <p className="date-style">$ {item.totalPrice}.00</p>
                            </Col>
        
                            <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} style={{ marginTop: "10px" }}>
                                <p className="items-order">{item.products.length} items</p>
                            </Col>
                            <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} style={{ marginTop: "5px" }}>
                               
                                    <div className="view-details" onClick={()=>handalClicked(item)}>
                                        VIEW DETAILS
                                    </div>
                               
                            </Col>
                        </div>
                        )
                    })
                }

               

                {/* <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} style={{ marginTop: "50px", display: "flex", justifyContent: "space-between" }}>
                    <p className="date-style">01 JULY 2021</p>
                    <p className="date-style">$ 30.00</p>
                </Col>

                <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} style={{ marginTop: "10px" }}>
                    <p className="items-order">2 items</p>
                </Col>
                <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} style={{ marginTop: "5px" }}>
                    <div className="view-details">
                        VIEW DETAILS
                    </div>

                </Col> */}
            </Row>

        </div>
    );
};

export default OrderHistory;