import React, { useState, useEffect } from 'react';
import { useGlobalState } from '../../../store'
import './Profile.css';
import { Row, Col, Modal, Popconfirm, Upload, Skeleton, Button } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import { Link, useHistory } from 'react-router-dom';
import { isMobile } from "react-device-detect";
import Header from '../Layout/Header';
import LogoutModalStyle from './LogoutModalStyle';
import ImgCrop from 'antd-img-crop';
import { setLoginStatus } from './../../../store/index';



const Profile = (props) => {

    const [logOutmodal, setLotOutModal] = useState(false);
    const [skLoading, setSkLoading] = useState(true);
    const [currentUser, setCurrentUser] = useGlobalState('currentUser');
    const [loginStatus, setLoginStatus] = useGlobalState('loginStatus');
    let [image, setImage] = useState(null);
    let history = useHistory();


    useEffect(() => {
        const user = localStorage.getItem("user")
        if(user){
            setCurrentUser(JSON.parse(user))   
        }else{
            setCurrentUser({})
        }
               
    }, [])
    
    const confirm = () => {
    
        history.push("/getStart")      
    }
    const cancel = (e) => {
    }

    const managePayment = () => {

        history.push({
            pathname: '/paymentMethod',
            state: {
                managePay: 'managePay'
            }
        })
    }

    const manageAddress = () => {
        history.push({
            pathname: '/myAddress',
            state: {
                manageAddr: 'manageAddr'
            }
        })
    }
    const showModal = () => {
        setLotOutModal(true);
    }

    const hideModal = () => {
        setLotOutModal(false);
    }

    const browserTest = () => {
        if (isMobile) {
            return (
                null
            )
        } else {
            return (
                <Header />
            )
        }
    }

    useEffect(() => {
        setTimeout(() => {
            setSkLoading(false)
        }, 2000)
    })

    const onImageChange = (e) => {

        // let img = e.target.files[0];
        // setImage(URL.createObjectURL(img));
       

        if (e.target.files && e.target.files[0]) {
            let img = e.target.files[0];
           

            setImage(URL.createObjectURL(img));
        }
    };

    return (
        <div >

            {browserTest()}

            <Row>
                <Col xs={{ offset: 2, span: 20 }} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} className="common-top-margin" >

                    <Skeleton loading={skLoading} avatar active paragraph={{ rows: 12 }}>
                        <div style={{ display: "flex", justifyContent: "center", flexDirection: "column", marginTop: "20px" }}>

                            <img src={image || currentUser?.image} className="image-style" />

                            <label style={{textAlign:"center",marginTop:"10px",cursor:"pointer"}} for="file-input"><UploadOutlined /></label>

                            
                                <input id="file-input" type="file" name="myImage" onChange={(e) => onImageChange(e)} style={{display:"none"}}/>
                           
                            <p className="name-styles"> {currentUser.fullName} </p>
                        </div>

                        <div className="sub-profile">
                            <p style={{ flexGrow: "1" }}> <img src="./image/vector9.png" /></p>
                            <p className="account-profile">Account and Profile </p>
                            <Link to="accountAndProfile"><p style={{ flexGrow: "1", textAlign: "right" }}>  <img src="./image/vector12.png" style={{ cursor: "pointer" }} /></p></Link>
                        </div>

                        <div className="sub-profile">
                            <p style={{ flexGrow: "1" }}> <img src="./image/vector11.png" /></p>
                            <p className="account-profile">Manage Payment Methods </p>
                            <p style={{ flexGrow: "1", textAlign: "right", cursor: "pointer" }}> <img src="./image/vector12.png" onClick={managePayment} /> </p>
                        </div>

                        <div className="sub-profile">
                            <p style={{ flexGrow: "1" }}> <img src="./image/vector13.png" /></p>
                            <p className="account-profile">Manage Addresses </p>
                            <p style={{ flexGrow: "1", textAlign: "right", cursor: "pointer" }}>  <img src="./image/vector12.png" onClick={manageAddress} /> </p>
                        </div>

                        <div className="sub-profile">
                            <p style={{ flexGrow: "1" }}> <img src="./image/vector14.png" /></p>
                            <p className="account-profile"> Order History </p>
                            <p style={{ flexGrow: "1", textAlign: "right" }}><Link to="/orderHistory" > <img src="./image/vector12.png" /> </Link></p>
                        </div>

                        <div className="sub-profile">
                            <p style={{ flexGrow: "1" }}> <img src="./image/vector15.png" /></p>
                            <p className="account-profile"> Contact Support </p>
                            <p style={{ flexGrow: "1", textAlign: "right" }}><Link to="/contactSupport"  > <img src="./image/vector12.png" /> </Link> </p>
                        </div>

                        <div className="sub-profile">
                            <p style={{ flexGrow: "1" }}> <img src="./image/vector16.png" /></p>
                            <p className="account-profile"> Refer to a Friend </p>
                            <p style={{ flexGrow: "1", textAlign: "right" }}><Link to="/referToFriend"  > <img src="./image/vector12.png" /> </Link> </p>
                        </div>

                        <div className="sub-profile">
                            <p style={{ flexGrow: "1" }}> <img src="./image/vector17.png" /></p>
                            <p className="account-profile"> Write a Review </p>
                            <p style={{ flexGrow: "1", textAlign: "right" }}><img src="./image/vector12.png" /></p>
                        </div>

                        <div className="sub-profile">
                            <p style={{ flexGrow: "1" }}> <img src="./image/vector18.png" /></p>
                            <p className="account-profile"> Terms and Conditions </p>
                            <p style={{ flexGrow: "1", textAlign: "right" }}><Link to="/termAndCondition"> <img src="./image/vector12.png" /> </Link></p>
                        </div>

                        <div className="sub-profile">
                            <p style={{ flexGrow: "1" }}> <img src="./image/vector19.png" /></p>
                            <p className="account-profile"> Privacy Policy </p>
                            <p style={{ flexGrow: "1", textAlign: "right" }}> <Link to="/privacy"> <img src="./image/vector12.png" /> </Link></p>
                        </div>

                        <div className="sub-profile">
                            <p style={{ flexGrow: "1" }}> <img src="./image/vector9.png" /></p>
                            <p className="account-profile"> Logout </p>
                            <Popconfirm
                                title="Are you sure to LogOut this application?"
                                onConfirm={confirm}
                                onCancel={cancel}
                                okText="Yes"
                                cancelText="No"
                            >
                                <p style={{ flexGrow: "1", textAlign: "right", cursor: "pointer" }}> <img src="./image/vector12.png" /></p>
                            </Popconfirm>

                        </div>
                    </Skeleton>
                </Col>
            </Row>
            <Modal

                visible={logOutmodal}

                okButtonProps={{
                    style: {
                        display: "none",
                    },
                }}
                cancelButtonProps={{
                    style: {
                        display: "none",
                    },
                }}
                closable={false}
            >

                <LogoutModalStyle modalHidden={hideModal} />


            </Modal>

        </div>
    );
};

export default Profile;