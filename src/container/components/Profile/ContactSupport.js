import React from 'react';
import './Profile.css';
import { Row, Col, Input } from 'antd';
import CustomerData from '../../../CustomerData.json'
import { isMobile } from "react-device-detect";
import Header from '../Layout/Header';
import { Link } from 'react-router-dom';

const ContactSupport = () => {

    const browserTest = () =>{
        if(isMobile){
            return(
               null
         )
        }else{
            return(
               <Header />
            )  
        }
    }

    return (
        <div >
          {browserTest()}
            <Row>
                <Col xs={{offset:2,span:20}} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} className="common-top-margin">
                   <Link to={ isMobile ? "/layout" : "/profile"}> <img src="./image/vector8.png" /></Link>
                </Col>
                <Col xs={{offset:2,span:20}} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} style={{ marginTop: "50px" }}>
                    <p className="acc-and-pro">CONTACT SUPPORT </p>

                </Col>

                {
                    CustomerData.map((items, index) => {
                        return (

                            items.contact.map((item, index) => {
                                return (
                                    <Col key={index} xs={{offset:2,span:20}} sm={{ offset: 4, span: 16 }} md={{ offset: 6, span: 12 }} lg={{ offset: 8, span: 8 }} style={{ marginTop: "20px", display: "flex" }}>
                                        <p className="delete-icon"> <img src={item.img} /> </p>
                                        <p className="media-style" > {item.media} </p>
                                    </Col>
                                )
                            })

                        )
                    })
                }



            </Row>

        </div>
    );
};

export default ContactSupport;