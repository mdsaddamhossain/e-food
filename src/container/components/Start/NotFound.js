import React from 'react';
import { Result, Button } from 'antd';
import { Row, Col } from 'antd';
import Header from '../Layout/Header';
import { isMobile } from 'react-device-detect';
import { Link } from 'react-router-dom';


const NotFound = () => {

    const browserTest = () => {
        if (isMobile) {
            return null;
        } else {
            return <Header />
        }
    }


    return (
        <div>

            {browserTest()}

            <Row justify="center">
                <Result
                    status="404"
                    title="404"
                    subTitle="Sorry, the page you visited does not exist."
                    extra={ <Link to="/layout"><Button type="primary">Back Home</Button> </Link>}
                />
            </Row>


        </div>
    );
};

export default NotFound;